#include "fish.h"

#ifndef HUNTER_H
#define HUNTER_H

class hunter : public fish
{
Q_OBJECT
  public:
    static QVector<hunter*> children;
    hunter();
    hunter(float, float);
    ~hunter();
    void eat();
    void makeChild();

public slots:
    void onTimeout();
    void onChase();
    void onSexChase();


signals:
    void chase();
    void makeMe(hunter*);
    void addMe(hunter*);
    void makeMeMove(hunter*);
    void removeMe(hunter*);

};

#endif // HUNTER_H
