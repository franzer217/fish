#ifndef GLOBALS_H
#define GLOBALS_H
#include <cmath>
#include <QTimer>

const int timeLoop = 20;
const int A = 1;
const double M_PI = 3.14;
const float F = M_PI/3;
const int eatRadius = 5;
const float sRadius = 5.0;
const float scenewidth = 900;
const float sceneheight = 550;
const int huntCount = 20;
const int herbCount = 40;
const int herbAgeModifier = 40;
const int huntAgeModifier = 35;
const int maxHuntAge = huntAgeModifier*139;
const int maxHerbAge = herbAgeModifier*139;


#endif // GLOBALS_H


