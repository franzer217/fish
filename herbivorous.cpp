#include "herbivorous.h"
#include "stdlib.h"
#include "globals.h"

herbivorous::herbivorous():fish()
{
  period = + M_PI/4;
  speed = 4.7;
  desire = 0;
  x = 0 + rand() % 50;
  y = 0 + rand() % 50;
  size = 10;
  radius = 100;
  sexRadius = 300;
  maxAge = maxHerbAge - maxHerbAge/10 + rand()%(maxHerbAge/5);
  isMale = rand()%2;
  incrDesire = (10 + rand()%6)/10000.0;
  if(isMale)
    color = QColor(0, 139, 0);
  else
    color = QColor(0, 255, 0);
  body = new QGraphicsEllipseItem(QRectF(x, y, size, size));//scene.addEllipse(QRectF(0, 0, 30, 40), QPen(Qt::green), QBrush(Qt::green));
  body->setBrush(color);
  body->setPen(QPen(Qt::black));
  target = 0;
  QObject::connect(this, SIGNAL(detection()), this, SLOT(onDetection()));
  QObject::connect(this, SIGNAL(sexChase()), this, SLOT(onSexChase()));

  allFish.push_back(this);
}

herbivorous::herbivorous(float x, float y):herbivorous()
{
  this->x = x;
  this->y = y;
  delete this->body;
  body = new QGraphicsEllipseItem(QRectF(x, y, size, size));
  body->setBrush(color);
  body->setPen(QPen(Qt::black));
}

void herbivorous::onSexChase()
{
  float L = sqrt(pow(target->getX() - getX(), 2) + pow(target->getY() - getY(), 2));
  if(L > sRadius)
  {
      float speedX = (target->getX() - getX())/L*this->getSpeed();
      float speedY = (target->getY() - getY())/L*this->getSpeed();
      setX(getX() + speedX);
      setY(getY() + speedY);
      body->setPos(body->x() + speedX,body->y() + speedY);
  }
  else
  {
      makeChild();
  }
}

void herbivorous::makeChild()
{
  if(!isMale)
  {
      children.push_back(new herbivorous(x, y));
      herbivorous* temp = children.last();
      emit makeMe(temp);
    }
  desire = 0;
  target = 0;
  noticed = 0;
}

void herbivorous::onTimeout()
{
  desire += incrDesire;
  age += 1;
  if(age > maxAge && noticed == false)
  {
    noticed = true;
    delete this;
    return;
  }
  if(!(age%herbAgeModifier) && age < maxHerbAge)
  {
    color = QColor(0, color.green()-1, 0);
    body->setBrush(QBrush(color));
  }
  if(!target)
  {
    target = chooseTarget();
  }
  if(target && target->metaObject()->className() != metaObject()->className())
    emit detection();
  if(!target && desire > 1)
    target = chooseSexTarget();
  if(target && target->metaObject()->className() == metaObject()->className())
    emit sexChase();
  if(!target)
  {
    if(counter==countTo)
    {
        countTo = rand()%499 + 1;
        counter = 0;
        alpha = (rand()%180-90)*M_PI/180;
        modifier = pow(-1, rand()%2);
    }
    float t = period/(1*timeLoop)*2*M_PI;
    float partX = cos(alpha+M_PI/2) * A * sin(t)-F*cos(alpha) * modifier;
    float partY = sin(alpha+M_PI/2)*A*sin(t)+F*sin(alpha);
    if ((getX() - partX)>=scenewidth-size||(getX() - partX)<=0)
    {
        partX = 0;
        alpha=alpha-M_PI/2;
    }
    if ((getY() + partY)>=sceneheight-size||(getY() + partY)<=0)
    {
        partY = 0;
        alpha=alpha-M_PI/2;
    }
    body->setPos(body->x() - partX, body->y() + partY);
    setX(getX() - partX);
    setY(getY() + partY);
    period += M_PI/4;
    counter++;
  }
}

herbivorous::~herbivorous()
{
  removeMe(this);
}

void herbivorous::onDetection()
{
  float L = sqrt(pow(target->getX() - getX(), 2) + pow(target->getY() - getY(), 2));
  float speedX = (target->getX() - getX())/L*this->getSpeed();
  float speedY = (target->getY() - getY())/L*this->getSpeed();
  if ((getX() - speedX)>=scenewidth-size||(getX() - speedX)<=0)
  {
      if((getX() - speedX)>=scenewidth-size)
      {
        body->setX(body->x() + (scenewidth - size - getX()));
        setX(scenewidth-size);
      }
      if((getX() - speedX)<=0)
      {
        body->setX(body->x() - getX());
        setX(0);
      }
  }
  else
  {
    setX(getX() - speedX);
    body->setX(body->x() - speedX);
  }
  if ((getY() - speedY)>=sceneheight-size||(getY() - speedY)<=0)
  {
      if((getY() - speedY)>=sceneheight-size)
      {
        body->setY(body->y() + (sceneheight-size - getY()));
        setY(sceneheight-size);
      }
      if((getY() - speedY)<=0)
      {
        body->setY(body->y() - getY());
        setY(0);
      }
  }
  else
  {
    setY(getY() - speedY);
    body->setY(body->y() - speedY);
  }
  target = 0;
}
