#include "widget.h"
#include "ui_widget.h"
#include <QSize>
#include "globals.h"

Widget::Widget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::Widget)
{
  ui->setupUi(this);
  view = new QGraphicsView(this);
  QSize f(scenewidth, sceneheight);
  this->setMaximumSize(f);
  this->setMinimumSize(f);
  view->setMinimumSize(f);
  scene = new QGraphicsScene(0, 0, scenewidth, sceneheight);
  view->setScene(scene);
  timer = new QTimer(this);
  timer->start(timeLoop);
  hunter* tempHun = NULL;
  herbivorous* tempHerb = NULL;
  for(int i = 0; i < huntCount; i++)
  {
    tempHun = new hunter;
    scene->addItem(tempHun->body);
    QObject::connect(timer, SIGNAL(timeout()), tempHun, SLOT(onTimeout()));
    QObject::connect(tempHun, SIGNAL(makeMeMove(hunter*)), this, SLOT(onMakeMeMove(hunter*)));
    QObject::connect(tempHun, SIGNAL(removeMe(hunter*)), this, SLOT(onRemoveMe(hunter*)));
    QObject::connect(tempHun, SIGNAL(addMe(hunter*)), this, SLOT(onAddMe(hunter*)));
    QObject::connect(tempHun, SIGNAL(makeMe(hunter*)), this, SLOT(onMakeMe(hunter*)));
  }
  for(int i = 0; i < herbCount; i++)
  {
    tempHerb = new herbivorous;
    scene->addItem(tempHerb->body);
    QObject::connect(timer, SIGNAL(timeout()), tempHerb, SLOT(onTimeout()));
    QObject::connect(tempHerb, SIGNAL(removeMe(herbivorous*)), this, SLOT(onRemoveMe(herbivorous*)));
    QObject::connect(tempHerb, SIGNAL(makeMeMove(herbivorous*)), this, SLOT(onMakeMeMove(herbivorous*)));
    QObject::connect(tempHerb, SIGNAL(addMe(herbivorous*)), this, SLOT(onAddMe(herbivorous*)));
    QObject::connect(tempHerb, SIGNAL(makeMe(herbivorous*)), this, SLOT(onMakeMe(herbivorous*)));
  }
}

void Widget::onRemoveMe(herbivorous* target)
{
  scene->removeItem(target->body);
}

void Widget::onRemoveMe(hunter* target)
{
  scene->removeItem(target->body);
}

void Widget::onMakeMeMove(hunter* target)
{
  QObject::connect(timer, SIGNAL(timeout()), target, SLOT(onTimeout()));
}

void Widget::onMakeMeMove(herbivorous* target)
{
  QObject::connect(timer, SIGNAL(timeout()), target, SLOT(onTimeout()));
}

void Widget::onAddMe(herbivorous* target)
{
  scene->addItem(target->body);
}

void Widget::onAddMe(hunter* target)
{
  scene->addItem(target->body);
}

void Widget::onMakeMe(hunter* target)
{
  onMakeMeMove(target);
  onAddMe(target);
  QObject::connect(target, SIGNAL(makeMe(hunter*)), this, SLOT(onMakeMe(hunter*)));
  QObject::connect(target, SIGNAL(removeMe(hunter*)), this, SLOT(onRemoveMe(hunter*)));
}

void Widget::onMakeMe(herbivorous* target)
{
  onMakeMeMove(target);
  onAddMe(target);
  QObject::connect(target, SIGNAL(makeMe(herbivorous*)), this, SLOT(onMakeMe(herbivorous*)));
  QObject::connect(target, SIGNAL(removeMe(herbivorous*)), this, SLOT(onRemoveMe(herbivorous*)));
}

Widget::~Widget()
{
  delete ui;
}
