#ifndef FISH_H
#define FISH_H
#include <QColor>
#include <QGraphicsEllipseItem>
#include <QObject>
#include <QPainter>
#include <QDebug>
#include <ctime>
#include <cmath>
#include "globals.h"
#include "string.h"
/*#include "herbivorous.h"
#include "hunter.h"*/




class fish : public QObject
{
  Q_OBJECT

  protected:
    float speed;
    float hunger;
    int age;
    int maxAge;
    int fertility;
    int isMale;
    float x;
    float y;
    QColor color;
    float size;
    int counter;
    int countTo;
    float alpha;
    int modifier;
    int radius;
    fish* target;
    float desire;
    int sexRadius;
    float incrDesire;


  public:
    bool noticed;
    static QVector<fish*> allFish;
    QGraphicsEllipseItem* body;
    double period;
    //static QGraphicsScene scene;
    fish();
    //fish(const fish copy);
    ~fish();
    void setSpeed(float a);
    float getSpeed();
    void setHunger(float a);
    float getHunger();
    void setAge(int a);
    int getAge();
    void setFertility(int a);
    int getFertility();
    void setIsMale(int a);
    int getIsMale();
    void setX(float a);
    float getX();
    void setY(float a);
    float getY();
    void setColor(QColor a);
    QColor getColor();
    fish* chooseTarget();
    fish* chooseSexTarget();
    void makeChild();

  signals:
    void iHaveNoticed();
    void sexChase();

  public slots:

};

#endif // FISH_H
