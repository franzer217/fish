#include "fish.h"
#include "stdlib.h"
#include "herbivorous.h"
#include "hunter.h"

#ifndef FISH_CPP
#define FISH_CPP

fish::fish():QObject()
{
  hunger = 1;
  age = 0;
  fertility = rand() % 100;
  isMale = rand()%2;
  counter = 0;
  countTo = 0;
  alpha = 0;
  modifier = 1;
  noticed = 0;
  maxAge = maxHuntAge - maxHuntAge/10 + rand()%(maxHuntAge/5);
}

fish* fish::chooseTarget()
{
  int centerX = x + size/2;
  int centerY = y + size/2;
  QVectorIterator<fish*> it(allFish);
  while(it.hasNext())
  {
      //qDebug() << it.peekNext()->body->x() << it.peekNext()->body->y();
      if(metaObject()->className() != it.peekNext()->metaObject()->className() && it.peekNext()->noticed == false && sqrt(pow(centerX - it.peekNext()->getX(), 2) + pow(centerY - it.peekNext()->getY(), 2)) <= radius)
      {
        if(!strcmp(it.peekNext()->metaObject()->className(),"herbivorous"))
          it.peekNext()->noticed = true;
        return it.peekNext();
      }
      else
        it.next();
   }
  return 0;
}

fish* fish::chooseSexTarget()
{
  int centerX = x + size/2;
  int centerY = y + size/2;
  QVectorIterator<fish*> it(allFish);
  while(it.hasNext())
  {
      if(metaObject()->className() == it.peekNext()->metaObject()->className() && (isMale != it.peekNext()->isMale && this != it.peekNext() && it.peekNext()->desire >= 1 - it.peekNext()->incrDesire && sqrt(pow(centerX - it.peekNext()->getX(), 2) + pow(centerY - it.peekNext()->getY(), 2)) <= sexRadius && !noticed &&  !it.peekNext()->noticed || this == it.peekNext()->target))
      {
        noticed = it.peekNext()->noticed = true;
        return it.peekNext();
      }
      else
        it.next();
   }
  return 0;
}

fish::~fish()
{
  allFish.erase(std::find(allFish.begin(), allFish.end(), this));
}

void fish::setSpeed(float a)
{
  speed = a;
}

float fish::getSpeed()
{
  return speed;
}

void fish::setHunger(float a)
{
  hunger = a;
}

float fish::getHunger()
{
  return hunger;
}

void fish::setAge(int a)
{
  age = a;
}

int fish::getAge()
{
  return age;
}

void fish::setFertility(int a)
{
  fertility = a;
}

int fish::getFertility()
{
  return fertility;
}

void fish::setIsMale(int a)
{
  isMale = a;
}

int fish::getIsMale()
{
  return isMale;
}

void fish::setX(float a)
{
  x = a;
}

float fish::getX()
{
  return x;
}

void fish::setY(float a)
{
  y = a;
}

float fish::getY()
{
  return y;
}

void fish::setColor(QColor a)
{
  color = a;
}

QColor fish::getColor()
{
  return color;
}

#endif // FISH_CPP
