#include "hunter.h"
#include "stdlib.h"

hunter::hunter():fish()
{
  period = M_PI/4;
  hunger = 1;
  desire = 0;
  speed = 5;
  x = scenewidth - 70.0 + float(rand() % 50);
  y = sceneheight - 70.0 + float(rand() % 50);
  radius = 80;
  sexRadius = 400;
  isMale = rand()%2;
  incrDesire = (4 + rand()%5)/10000.0;
  if(isMale)
    color = QColor(139, 0, 0);
  else
    color = QColor(255, 0, 0);
  size = 15;
  body = new QGraphicsEllipseItem(QRectF(x, y, size, size));
  body->setBrush(color);
  body->setPen(QPen(Qt::black));
  target = 0;
  QObject::connect(this, SIGNAL(chase()), this, SLOT(onChase()));
  QObject::connect(this, SIGNAL(sexChase()), this, SLOT(onSexChase()));
  allFish.push_back(this);
}

hunter::hunter(float x, float y) :hunter()
{
  this->x = x;
  this->y = y;
  delete this->body;
  body = new QGraphicsEllipseItem(QRectF(x, y, size, size));
  body->setBrush(color);
  body->setPen(QPen(Qt::black));
}

void hunter::onTimeout()
{
  hunger += 0.001;
  age += 1;
  if(age > maxAge && noticed == false)
  {
    noticed = true;
    delete this;
    return;
  }
  if(!(age%huntAgeModifier) && age < maxHuntAge)
  {
    color = QColor(color.red()-1, 0, 0);
    body->setBrush(QBrush(color));
  }
  desire += incrDesire;
  if(!target&&hunger > 1)
  {
    target = chooseTarget();
  }
  if(target && target->metaObject()->className() != metaObject()->className())
    emit chase();
  if(!target && desire > 1)
    target = chooseSexTarget();
  if(target && target->metaObject()->className() == metaObject()->className())
    emit sexChase();
  if(!target)
  {
    if(counter==countTo)
    {
        countTo = rand()%499 + 1;
        counter = 0;
        alpha = (rand()%180-90)*M_PI/180;
        modifier = pow(-1, rand()%2);
    }  
    float t = period/(1*timeLoop)*2*M_PI;
    float partX = cos(alpha+M_PI/2) * A * sin(t)-F*cos(alpha) * modifier;
    float partY = sin(alpha+M_PI/2)*A*sin(t)+F*sin(alpha);
    if ((getX() - partX)>=scenewidth-size||(getX() - partX)<=0)
    {
        partX = 0;
        alpha=alpha-M_PI/2;
    }
    if ((getY() + partY)>=sceneheight-size||(getY() + partY)<=0)
    {
        partY = 0;
        alpha=alpha-M_PI/2;
    }
    body->setPos(body->x() - partX, body->y() + partY);
    setX(getX() - partX);
    setY(getY() + partY);
    period += M_PI/4;
    counter++;
  }

}

void hunter::onChase()
{
  float L = sqrt(pow(target->getX() - getX(), 2) + pow(target->getY() - getY(), 2));
  if(L > eatRadius)
  {
      float speedX = (target->getX() - getX())/L*this->getSpeed();
      float speedY = (target->getY() - getY())/L*this->getSpeed();
      setX(getX() + speedX);
      setY(getY() + speedY);
      body->setPos(body->x() + speedX,body->y() + speedY);
  }
  else
  {
    eat();
    if(getX()>=scenewidth-size)
    {
      body->setX(body->x() + (scenewidth - size - getX()));
      setX(scenewidth-size);
    }
    if(getX()<=0)
    {
      body->setX(body->x() - getX());
      setX(0);
    }
    if((getY())>=sceneheight-size)
    {
      body->setY(body->y() + (sceneheight-size - getY()));
      setY(sceneheight-size);
    }
    if((getY())<=0)
    {
      body->setY(body->y() - getY());
      setY(0);
    }
  }
}

void hunter::eat()
{
  delete target;
  target = NULL;
  hunger = 0;
}

void hunter::onSexChase()
{
  float L = sqrt(pow(target->getX() - getX(), 2) + pow(target->getY() - getY(), 2));
  if(L > sRadius)
  {
      float speedX = (target->getX() - getX())/L*this->getSpeed();
      float speedY = (target->getY() - getY())/L*this->getSpeed();
      setX(getX() + speedX);
      setY(getY() + speedY);
      body->setPos(body->x() + speedX,body->y() + speedY);
  }
  else
  {
      makeChild();
  }
}

void hunter::makeChild()
{
  if(!isMale)
  {
      children.push_back(new hunter(x, y));
      hunter* temp = children.last();
      emit makeMe(temp);
  }
  desire = 0;
  target = 0;
  noticed = 0;
}

hunter::~hunter(){
  emit removeMe(this);
}


