#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsView>
#include <QTimer>
#include "herbivorous.h"
#include "hunter.h"

namespace Ui {
  class Widget;
}

class Widget : public QWidget
{
  Q_OBJECT

public:
  explicit Widget(QWidget *parent = 0);
  ~Widget();

  QGraphicsView* view;
  QGraphicsScene* scene;
  QTimer* timer;


  Ui::Widget *ui;

public slots:
  void onRemoveMe(herbivorous*);
  void onRemoveMe(hunter*);
  void onAddMe(herbivorous*);
  void onAddMe(hunter*);
  void onMakeMeMove(hunter* target);
  void onMakeMeMove(herbivorous* target);
  void onMakeMe(hunter* target);
  void onMakeMe(herbivorous* target);
};

#endif // WIDGET_H
