#include "fish.h"

#ifndef HERBIVOROUS_H
#define HERBIVOROUS_H

class herbivorous : public fish
{
Q_OBJECT
public:
    static QVector<herbivorous*> children;
    herbivorous();
    herbivorous(float, float);
    ~herbivorous();
    void makeChild();
public slots:
    void onTimeout();
    void onDetection();
    void onSexChase();
signals:
    void removeMe(herbivorous*);
    void detection();
    void addMe(herbivorous*);
    void leaveStep(herbivorous*);
    void makeMeMove(herbivorous*);
    void makeMe(herbivorous*);
};

#endif // HERBIVOROUS_H
