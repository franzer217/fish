#include "widget.h"
#include <QApplication>
#include "hunter.h"
#include "herbivorous.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include "fish.h"
//#include <QWidget>
QVector<fish*> fish::allFish(0);
QVector<hunter*> hunter::children(0);
QVector<herbivorous*> herbivorous::children(0);

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  srand(time(0));
  Widget *w = new Widget();

  w->show();
  return a.exec();
}\
