/****************************************************************************
** Meta object code from reading C++ file 'herbivorous.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../herbivorous.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'herbivorous.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_herbivorous_t {
    QByteArrayData data[12];
    char stringdata[112];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_herbivorous_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_herbivorous_t qt_meta_stringdata_herbivorous = {
    {
QT_MOC_LITERAL(0, 0, 11), // "herbivorous"
QT_MOC_LITERAL(1, 12, 8), // "removeMe"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 12), // "herbivorous*"
QT_MOC_LITERAL(4, 35, 9), // "detection"
QT_MOC_LITERAL(5, 45, 5), // "addMe"
QT_MOC_LITERAL(6, 51, 9), // "leaveStep"
QT_MOC_LITERAL(7, 61, 10), // "makeMeMove"
QT_MOC_LITERAL(8, 72, 6), // "makeMe"
QT_MOC_LITERAL(9, 79, 9), // "onTimeout"
QT_MOC_LITERAL(10, 89, 11), // "onDetection"
QT_MOC_LITERAL(11, 101, 10) // "onSexChase"

    },
    "herbivorous\0removeMe\0\0herbivorous*\0"
    "detection\0addMe\0leaveStep\0makeMeMove\0"
    "makeMe\0onTimeout\0onDetection\0onSexChase"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_herbivorous[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       4,    0,   62,    2, 0x06 /* Public */,
       5,    1,   63,    2, 0x06 /* Public */,
       6,    1,   66,    2, 0x06 /* Public */,
       7,    1,   69,    2, 0x06 /* Public */,
       8,    1,   72,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   75,    2, 0x0a /* Public */,
      10,    0,   76,    2, 0x0a /* Public */,
      11,    0,   77,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void herbivorous::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        herbivorous *_t = static_cast<herbivorous *>(_o);
        switch (_id) {
        case 0: _t->removeMe((*reinterpret_cast< herbivorous*(*)>(_a[1]))); break;
        case 1: _t->detection(); break;
        case 2: _t->addMe((*reinterpret_cast< herbivorous*(*)>(_a[1]))); break;
        case 3: _t->leaveStep((*reinterpret_cast< herbivorous*(*)>(_a[1]))); break;
        case 4: _t->makeMeMove((*reinterpret_cast< herbivorous*(*)>(_a[1]))); break;
        case 5: _t->makeMe((*reinterpret_cast< herbivorous*(*)>(_a[1]))); break;
        case 6: _t->onTimeout(); break;
        case 7: _t->onDetection(); break;
        case 8: _t->onSexChase(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< herbivorous* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< herbivorous* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< herbivorous* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< herbivorous* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< herbivorous* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (herbivorous::*_t)(herbivorous * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::removeMe)) {
                *result = 0;
            }
        }
        {
            typedef void (herbivorous::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::detection)) {
                *result = 1;
            }
        }
        {
            typedef void (herbivorous::*_t)(herbivorous * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::addMe)) {
                *result = 2;
            }
        }
        {
            typedef void (herbivorous::*_t)(herbivorous * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::leaveStep)) {
                *result = 3;
            }
        }
        {
            typedef void (herbivorous::*_t)(herbivorous * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::makeMeMove)) {
                *result = 4;
            }
        }
        {
            typedef void (herbivorous::*_t)(herbivorous * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&herbivorous::makeMe)) {
                *result = 5;
            }
        }
    }
}

const QMetaObject herbivorous::staticMetaObject = {
    { &fish::staticMetaObject, qt_meta_stringdata_herbivorous.data,
      qt_meta_data_herbivorous,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *herbivorous::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *herbivorous::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_herbivorous.stringdata))
        return static_cast<void*>(const_cast< herbivorous*>(this));
    return fish::qt_metacast(_clname);
}

int herbivorous::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = fish::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void herbivorous::removeMe(herbivorous * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void herbivorous::detection()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void herbivorous::addMe(herbivorous * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void herbivorous::leaveStep(herbivorous * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void herbivorous::makeMeMove(herbivorous * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void herbivorous::makeMe(herbivorous * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
